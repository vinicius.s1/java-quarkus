# Teste para Java Developer

### tecnologia
  - maven - repositório
  - java
  - quarkus - framework
  - panache/hibernate - facilita o mapeamento das entidades
  - mysql -  database
  - openfeign -  utilizado no metodo para baixar do google api e para usar mapeamento com @
  - apiGoogle - para interagir com a biblioteca do google.
  - quarkus oauth controle de acesso.



### tempo de trabalho
14/04/2020  - 18:20 to 22:00
15/04/2020  - 18:00 to 22:00
16/04/2020  - 18:00 to 22:00
17/04/2020  - 18:00 to 21:00
18/04/2020  - 12:00 pm to 03:00 am
19/04/2020  - 01:00 pm to 22:00

## problemas

windows 10 na versão home, tem problemas para habilitar 
hyper-v e instalar o docker, na versão nova não instala e na antiga da erro ao startar 

Google api esta criando token para o usuário e retornando status 201 ao criar favorito, porem não aparece o item salvo
Obs: essa parte não é necessária, porém fica de exemplo e estudo.




## Realizações
 - Criaçao do projeto via linha de comando e importação do mesmo via maven project
 - Instalação do mysql na maquina para poder rodar com as configurações no application,.properties
 - inserindo controller, serviços  e respositorios para trabalhar com livros
 - Inserir segurança de acesso básico.
 
## Comandos
 - rodar em debug com ./mvnw compile quarkus:dev -Ddebug -X
 - rodar em debug sem detalhes ./mvnw compile quarkus:dev -Ddebug 
 - rodar teste : ./mvnw clean test 