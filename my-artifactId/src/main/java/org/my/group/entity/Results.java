package org.my.group.entity;

import java.util.List;

public class Results {
	int totalItems;
	List<Result> items;
	int statusCode;

	public int getTotalItems() {
		return totalItems;
	}

	public List<Result> getItems() {
		return items;
	}
	
	public int getStatusCode() {
		return statusCode;
	}
}
