package org.my.group.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import static javax.persistence.GenerationType.SEQUENCE;

import java.util.List;

@javax.persistence.Entity
@javax.persistence.Table(name = "Book")
public class Book  extends PanacheEntityBase {

	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "SEQ_BOOK")
	@SequenceGenerator(name = "SEQ_BOOK", sequenceName = "SEQ_BOOK", allocationSize = 1)
	@Column(name = "id")
	private Long id;
	@Column(name = "idBook")
	private Long idBook;
	@Column(name = "title")
	private String title;
	
	@ElementCollection
	@Column(name = "authors")
	private List<String> authors;

	@Column(name = "favorite")
	private boolean favorite;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setIdBook(Long idBook) {
		this.idBook = idBook;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	public boolean isFavorite() {
		return favorite;
	}

	public void setFavorite(boolean favorite) {
		this.favorite = favorite;
	}

	public String getTitle() {
		return title;
	}

	public List<String> getAuthors() {
		return authors;
	}

	public long getIdBook() {
		return idBook;
	}

	public Book() {
	}

	public Book(long idBook, String title, List<String> authors) {
		this.idBook = idBook;
		this.title = title;
		this.authors = authors;
	}
}
