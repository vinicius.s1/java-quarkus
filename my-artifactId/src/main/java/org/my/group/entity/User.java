package org.my.group.entity;

import static javax.persistence.GenerationType.SEQUENCE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@javax.persistence.Entity
@javax.persistence.Table(name = "User")
public class User extends PanacheEntityBase {

	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "SEQ_BOOK")
	@SequenceGenerator(name = "SEQ_BOOK", sequenceName = "SEQ_BOOK", allocationSize = 1)
	@Column(name = "id")
	private Long id;
	@Column(name = "name")
	String name;
	@Column(name = "password")
	String password;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public User() {

	}

	public User(String name, String password) {
		this.name = name;
		this.password = password;
	}
}
