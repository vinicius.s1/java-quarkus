package org.my.group.service.interfaces;

import java.util.Map;

import org.my.group.entity.Results;
import org.springframework.web.bind.annotation.RequestBody;

import feign.Body;
import feign.Headers;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;
import feign.Response;

public interface IGoogleBooksApi {
	@RequestLine("GET /books/v1/volumes?q=&startIndex=&maxResults=&key=")
	Results findBookByName(@QueryMap Map<String, Object> queryParameters);

	@RequestLine("POST /books/v1/mylibrary/bookshelves/0/addVolume?volumeId=&key=")
	@Headers({ "Content-Type: application/json", "Authorization: Bearer {access_token}","Content-Length:0" })
	Response postFavorite(@QueryMap Map<String, Object> queryParameters, @Param("access_token") String access_token, @RequestBody String content);
}
