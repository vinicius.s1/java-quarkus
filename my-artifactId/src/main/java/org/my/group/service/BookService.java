package org.my.group.service;

import java.util.Collections;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.my.group.entity.Book;
import org.my.group.entity.Result;
import org.my.group.repository.BookRepository;
import org.my.group.service.interfaces.IBookService;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;

import io.quarkus.panache.common.Page;

@ApplicationScoped
public class BookService implements IBookService {

	@Inject
	private BookRepository bookRepository;

	public List<Book> getBooks(int page, int maxResults) {
		List<Book> result =  (List<Book>) bookRepository.listAll();
		result.sort((o1, o2) -> o1.getId().compareTo(o2.getId()));
		return getPage(result, page, maxResults);
	}

	@Transactional
	public void addBookFavorite(Book book) throws Exception {
		Book b = bookRepository.findById(book.getId());
		if (b == null)
			throw new Exception();
		b.setFavorite(book.isFavorite());
		bookRepository.persist(b);
	}

	@Transactional
	public void addBook(Book book) {
		bookRepository.persist(book);
	}

	public List<Book> getBookFavorite(int page, int maxResults) {
		List<Book> result = (List<Book>) bookRepository.list("favorite", true);
		result.sort((o1, o2) -> o1.getId().compareTo(o2.getId()));
		return getPage(result, page, maxResults);

	}

	public List<Book> getPage(List<Book> sourceList, int page, int pageSize) {
		if (pageSize <= 0 || page <= 0) {
			throw new IllegalArgumentException("invalid page size: " + pageSize);
		}

		int fromIndex = (page - 1) * pageSize;
		if (sourceList == null || sourceList.size() < fromIndex) {
			return Collections.emptyList();
		}

		// toIndex exclusive
		return sourceList.subList(fromIndex, Math.min(fromIndex + pageSize, sourceList.size()));
	}

	@Transactional
	public void delete(long id) throws Exception {
		Book b = bookRepository.findById(id);
		if (b == null)
			throw new Exception();
		b.setFavorite(false);
		bookRepository.persist(b);
	}

}
