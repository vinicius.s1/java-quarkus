package org.my.group.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.my.group.entity.Book;
import org.my.group.entity.Result;
import org.my.group.entity.Results;
import org.my.group.service.interfaces.IGoogleBooksApi;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.common.io.Resources;

import feign.Feign;
import feign.Logger;
import feign.Response;
import feign.gson.GsonDecoder;

public class BookLookupService {

	public Book fetchBookByName(String name, int startIndex, int maxResults, String key) throws BookLookupException {
		final IGoogleBooksApi googleBooksApi = connect();
		final Map<String, Object> queryParameters = new HashMap<>();
		queryParameters.put("q", "" + name);
		queryParameters.put("startIndex", "" + startIndex);
		queryParameters.put("maxResults", "" + maxResults);
		queryParameters.put("key", "" + key);
		final Results apiResponse = googleBooksApi.findBookByName(queryParameters);
		if (apiResponse == null || apiResponse.getTotalItems() < 1) {
			throw new BookLookupException("No books found for Name " + name);
		}
		final List<Result> results = apiResponse.getItems();
		if (results == null || results.size() < 1) {
			throw new BookLookupException("Invalid items list for N " + name);
		}
		final Book book = results.get(0).getBook();
		return book;
	}
	
	
	
	public List<Result> fetchBookManyByName(String name, int startIndex, int maxResults, String key) throws BookLookupException {
		final IGoogleBooksApi googleBooksApi = connect();
		final Map<String, Object> queryParameters = new HashMap<>();
		queryParameters.put("q", "" + name);
		queryParameters.put("startIndex", "" + startIndex);
		queryParameters.put("maxResults", "" + maxResults);
		queryParameters.put("key", "" + key);
		final Results apiResponse = googleBooksApi.findBookByName(queryParameters);
		if (apiResponse == null || apiResponse.getTotalItems() < 1) {
			throw new BookLookupException("No books found for Name " + name);
		}
		final List<Result> results = apiResponse.getItems();
		if (results == null || results.size() < 1) {
			throw new BookLookupException("Invalid items list for N " + name);
		}
		 return  results;
	}

	public int postFavorite(String volumeId, String key, String token) throws BookLookupException {
		final IGoogleBooksApi googleBooksApi = connect();
		final Map<String, Object> queryParameters = new HashMap<>();
		queryParameters.put("volumeId", "" + volumeId);
		queryParameters.put("key", "" + key);
        String content =  "{\"\": \"\"}";
		final Response apiResponse = googleBooksApi.postFavorite(queryParameters, token, content);
		if (apiResponse.status() != 204) {
			throw new BookLookupException("No book insert  for id " + volumeId);
		}
		if (apiResponse.status() != 204) {
			throw new BookLookupException("Invalid items list for N " + volumeId);
		}
		return apiResponse.status();
	}

	private static IGoogleBooksApi connect() {
		return Feign.builder().decoder(new GsonDecoder()).logger(new Logger.ErrorLogger()).logLevel(Logger.Level.BASIC)
				.target(IGoogleBooksApi.class, "https://www.googleapis.com");
	}

	public String createClientToken() {

		try {

			URL url = Resources.getResource("ethereal-smoke-274701-6949a8845b05.json");

			GoogleCredentials credentials = null;
			try {
				credentials = GoogleCredentials.fromStream(new FileInputStream(url.getPath()));
			} catch (IOException e) {
				return "Error";
			}

			if (credentials.createScopedRequired()) {
				credentials = credentials
						.createScoped(Collections.singletonList("https://www.googleapis.com/auth/books"));
			}

			try {
				credentials.refreshIfExpired();
			} catch (IOException e) {
				return "Error";
			}
			return credentials.getAccessToken().getTokenValue();
		} catch (Exception ex) {
			return "Error";
		}
	}
}