package org.my.group.service.interfaces;

import java.util.List;

import org.my.group.entity.User;

public interface IUserService {

	public List<User> getUsers();

	public void addUser(User user);

	public User getUser(int id);

	public long delete(int id);

}
