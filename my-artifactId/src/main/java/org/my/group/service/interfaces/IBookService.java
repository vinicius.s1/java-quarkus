package org.my.group.service.interfaces;

import java.util.List;

import org.my.group.entity.Book;

public interface IBookService {

	public List<Book> getBooks(int page, int maxResults);

	public void addBookFavorite(Book book) throws Exception;
	
	
	public void addBook(Book book);


	public List<Book> getBookFavorite(int page, int maxResults);

	public void delete(long id) throws Exception;

}
