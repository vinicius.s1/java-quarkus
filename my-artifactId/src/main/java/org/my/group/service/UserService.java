package org.my.group.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.my.group.entity.User;
import org.my.group.repository.UserRepository;

@ApplicationScoped
public class UserService {

	@Inject
	private UserRepository userRepository;

	public List<User> getUsers() {
		return userRepository.listAll();
	}

	@Transactional
	public void addUser(User user) {
		userRepository.persist(user);
	}

	public User getUser(String name, String password){
	    	String   query = "select id, name, password from User where name in (:name) and password  in (:password)";
	    	Map<String, Object> params = new HashMap<>();
	    	params.put("name", name);
	    	params.put("password", password);
	    	return userRepository.find("name = :name and password = :password", params).firstResult();

	    }

	public long delete(int id) {
		String query = "delete  from User where id = :id";
		return userRepository.delete(query, id);
	}

}
