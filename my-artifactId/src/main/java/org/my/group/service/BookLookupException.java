package org.my.group.service;

public class BookLookupException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BookLookupException(String errorMessage) {
		super(errorMessage);
	}
}
