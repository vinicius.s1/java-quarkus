package org.my.group.controller;

import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.my.group.entity.User;
import org.my.group.service.UserService;


@ApplicationScoped
@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserController {
	public static Logger logger=Logger.getLogger("global");

	    @Inject
	    private UserService userService;

	    @GET
	    @Produces(MediaType.TEXT_PLAIN)
	    public String hello() {
	        return "hello, this is begin with Quarkus!!!";
	    }

	    @POST
	    @Path("/user")
	    @Produces("application/json")
	    public Response addUser(User user){
	        userService.addUser(user);
	        return Response.ok().build();
	    }

	    @GET
	    @Path("/users")
	    @Produces("application/json")
	    public Response getUsers(){
	        List<User> user = userService.getUsers();
	        return Response.ok(user).build();
	    }
	    
	    
	    @GET
	    @Path("/users/{id}/{name}/{password}")
	    @Produces("application/json")
	    public Response getUse(@PathParam("id") int id, @PathParam("name") String name, @PathParam("password") String password){
	       User user = userService.getUser(name, password);
	        return Response.ok(user).build();
	    }
	    
	    
	    
	    @DELETE
	    @Path("/user/{id}")
	    @Produces("application/json")
	    public Response deleteUser(int id){
	        long  returnId = userService.delete(id);
	        return Response.ok(returnId).build();
	    }
	}
