package org.my.group.controller;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.my.group.entity.Book;
import org.my.group.entity.Result;
import org.my.group.entity.User;
import org.my.group.service.BookService;
import org.my.group.service.UserService;

@ApplicationScoped
@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BookController {
	public static Logger logger = Logger.getLogger("global");

	@Inject
	private BookService bookService;

	@Inject
	private UserService userService;

	@POST
	@Path("/books/favorite/{name}/{password}")
	@Produces("application/json")
	public Response addBookFavorite(Book book, @PathParam("name") String name, @PathParam("password") String password) throws Exception {
		bookService.addBookFavorite(book);
		return Response.ok().build();
	}

	@POST
	@Path("book")
	@Produces("application/json")
	public Response addBook(Book book) {
		bookService.addBook(book);
		return Response.ok().build();
	}

	@GET
	@Path("/books/{page}/{maxResults}/{name}/{password}")
	@Produces("application/json")
	public Response getBook(@PathParam("page") int page, @PathParam("maxResults") int maxResults, @PathParam("name") String name, @PathParam("password") String password) {
		User user = userService.getUser(name, password);
		if(user == null) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		List<Book> book = bookService.getBooks(page, maxResults);
		return Response.ok(book).build();
	}

	@GET
	@Path("/with-stars/{page}/{maxResults}/{name}/{password}")
	@Produces("application/json")
	public Response getFavorite(@PathParam("page") int page, @PathParam("maxResults") int maxResults, @PathParam("name") String name, @PathParam("password") String password) {
		
		User user = userService.getUser(name, password);
		if(user == null) {
			return Response.status(Status.FORBIDDEN).build();
		}
		List<Book> book = bookService.getBookFavorite(page, maxResults);
		return Response.ok(book).build();
	}

	@DELETE
	@Path("/books/favorite/{name}/{password}")
	@Produces("application/json")
	public Response deleteBook(long id, @PathParam("name") String name, @PathParam("password") String password) throws Exception {
		User user = userService.getUser(name, password);
		if(user == null) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		bookService.delete(id);
		return Response.ok().build();
	}
}
