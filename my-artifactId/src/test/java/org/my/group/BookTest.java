package org.my.group;

import io.quarkus.test.junit.QuarkusTest;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.my.group.entity.Book;
import org.my.group.entity.Result;
import org.my.group.entity.Results;
import org.my.group.service.BookLookupException;
import org.my.group.service.BookLookupService;

import com.fasterxml.jackson.databind.ObjectMapper;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@TestMethodOrder(OrderAnnotation.class)
@QuarkusTest
public class BookTest {
	ObjectMapper mapper = new ObjectMapper();
	private BookLookupService bookLookupService = new BookLookupService();
	private String api_key = "AIzaSyAvQgjuwjEVqV_i3hU2Wl0OdWvBdBGY10M";
	private String client_id = "905240117521-77nk9dg67p1obh2tp2jtkbnokap3a0kf.apps.googleusercontent.com";
	private String secret = "7kgpZwqrYoJl0Fz0epfagsov";

	
	
	@Test
	@Order(1)
	public void testSaveListOnDBnGoogleApi() {
		try {
			List<Result> items = bookLookupService.fetchBookManyByName("harry potter", 1, 30, api_key);
			for (Result result : items) {
				Book book = new Book(0, result.getBook().getTitle(), result.getBook().getAuthors());
				given().contentType("application/json").body(book).post("/api/book").then().statusCode(200);
			}
			
			//testSaveFavoriteApi();
		} catch (BookLookupException e) {
		}
	}
	
	
	@Test
	@Order(2)
	public void testCreateUserEndpoint() {

		Map<String, String> user = new HashMap<>();

		user.put("name", "vinicius");
		user.put("password", "teste123");

		given().contentType("application/json").body(user).post("/api/user").then().statusCode(200);
	}
	

	@Test
	@Order(3)
	public void testCreateUserErrorEndpoint() {

		Map<String, String> user = new HashMap<>();

		user.put("named", "vinicius");
		user.put("password", "teste123");

		given().contentType("application/json").body(user).post("/api/user").then().statusCode(400);
	}

	
	@Test
	@Order(4)
	public void testSaveFavoriteApi() {
		Book book = new Book();
		book.setId((long) 1);
		book.setFavorite(true);
		book.setIdBook((long) 0);
		book.setTitle("Harry Potter and the Bible");
		given().contentType("application/json").body(book).post("api/books/favorite/{name}/{password}", "vinicius","teste123").then().statusCode(200);
	}


	@Test
	@Order(5)
	public void testGetFavoriteApi() {
		given().contentType("application/json").get("api/with-stars/{page}/{maxResults}/{name}/{password}",1,5,"vinicius","teste123").then().body("[0].favorite", is(true));
	}
	
	@Test
	@Order(6)
	public void testGetAllBooksApi() {
		given().contentType("application/json").get("/api/books/{page}/{maxResults}/{name}/{password}",3,4,"vinicius","teste123").then().body("[0].title", is("Friends and Foes of Harry Potter"));
	}
	
	
	
	@Test
	@Order(6)
	public void testRemoveFavoriteApi() {
		given().contentType("application/json").body(1).delete("api/books/favorite/{name}/{password}","vinicius","teste123").then().statusCode(200);
	}
	


	
	

	// estes testes abaixo, permanecem como estudo e aprendizado, porem não é
	// necessário neste contexto do teste

	@Test
	public void testGenerateTokenGoogleApi() throws IOException {
		assertNotNull(bookLookupService.createClientToken(), "retorno não pode ser null");
	}

	@Test
	public void testCreateBookFavoriterEndpoint() {
		try {
			String token = bookLookupService.createClientToken();
			assertEquals(bookLookupService.postFavorite("z_JRBQAAQBAJ", api_key, token), 204);
		} catch (BookLookupException e) {
			e.printStackTrace();
		}
	}

}